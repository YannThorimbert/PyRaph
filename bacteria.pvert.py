import math
import matplotlib.pyplot as plt

"""
const plint nx = 2000;
const plint overlap_cells = 50;
const plint nx_sw = 1900;
const plint nx_fs = nx - nx_sw + overlap_cells;
"""

nx = 800

L = 14000
realDx = float(L)/nx

def get_x(x):
#    return -x - nx
#    return -x
#    return -x -100
    return x

def bed(x):
    x_fs = get_x(x)
    return (10. + 40.*x_fs/nx + 10*math.sin(math.pi*(4.*x_fs/nx - 0.5)))
    
def water(x):
    x_fs = get_x(x)
    return (50.5 - 40.*x_fs/nx - 10*math.sin(math.pi*(4.*x_fs/nx - 0.5)));
    
bedx = [bed(x) for x in range(nx)]
minbedx = min(bedx)
waterx = [water(x)/50.5 for x in range(nx)]
#waterx = [waterx[i]+bedx[i]-minbedx for i in range(nx)]
bedx = [v-minbedx for v in bedx]

print("bedOffset = ", -minbedx)

a = [water(x)/50.5 for x in range(-nx,nx)]
b = [water(-x)/50.5 for x in range(-nx,nx)]
delta = (water(0) - water(-nx)) / 50.5
c = [water(-x)/50.5 + delta for x in range(-nx,nx)]
#plt.plot(bedx)
plt.plot(list(range(-nx,nx)), a)
plt.plot(list(range(-nx,nx)), [water(x+100)/50.5 for x in range(-nx,nx)])
#plt.plot(list(range(-nx,nx)), b)
#plt.plot(list(range(-nx,nx)), c)
plt.grid()
    
    
#probleme : realDx trop grand, et discretisation (int) foire
    
    
#shift gauche-droite ?