# -*- coding: utf-8 -*-
import random
import time
from matplotlib.pyplot import *

def generate_random_list(length, rng=(1, 100), typ=int):
    """Return a list that does not necessarily contain any major element."""
    return [typ(random.randint(rng[0], rng[1])) for i in range(length)]

def generate_maj_list(length, major_value=3, rng=(1, 100), typ=int, m="auto"):
    """length: length of the list
    major_value: value of the major element
    rng: range of the values into the list
    typ: type of the values into the list
    m: number of times the major element appears. (if a float is given, then
        number(n) = int(m*n))
    """
    A = []
    major_value = typ(major_value)
    if m is "auto":
        m = length / 2 + 1
    elif isinstance(m, float):
        m = m * length
    m = int(m)
    for i in range(m):
        A.append(major_value)
    for i in range(length-m):
        A.append(typ(random.randint(rng[0], rng[1])))
    random.shuffle(A)
    return A

def naive_major(A):
    m = len(A) // 2 + 1 # '//' veut dire 'division entiere'.
    for el in A:
        if A.count(el) >= m:
            return el
    return None

def is_major(A, el):
    m=len(A)//2+1
    if A.count(el)>=m:
        return True
    else:
        return False
        

    
def reduce_list(A):
    """Ne marche que sur des listes avec un nombre pair d'élément"""
    l=len(A)
    B=[]
    for i in range(l//2):
       if A[2*i]==A[2*i+1]:
            B+=[A[2*i]]
    return B
       



    
def get_major(A):
    Acopy=list(A)
    
    while len(Acopy)>=2:
        l=len(Acopy)
        if l%2==1: #Si la liste a un nombre impaire d'éléments
            if is_major(Acopy, Acopy[-1]):
                Acopy=[Acopy[-1]]#Si le dernier élément est majoritaire, on le garde uniquement lui
            else:
                Acopy=Acopy[:-1]#On vire le dernier élément
        else:
            Acopy=reduce_list(Acopy)

    if len(Acopy)==0:
        return (None)
    else:
        if is_major(A, Acopy[0]):
            return Acopy[0]
        else:
            return (None)
            
            
            
        

    
######################################################################
#La fin ne sert que pour les exercices 1.4 et 1.5 (mais si quelqu'un a une envie soudaine de regarder ce qui cloche dans mon graphique, je ne lui en voudrai pas) 


def moyenne(L,N=10): #La moyenne de temps pour une liste a L éléments en utilisant N fois l'algorithme DaC
    lists = [generate_maj_list(L, major_value=7) for i in range(N)]
    begin=time.clock()
    for i in range(N):
        major=get_major(lists[i])
    end=time.clock()
    tempsmoyen=(end-begin)/N
    return(tempsmoyen)

def moyennenaive(L,N=10):
    lists = [generate_maj_list(L, major_value=7) for i in range(N)]
    begin=time.clock()
    for i in range(N):
        major=naive_major(lists[i])
    end=time.clock()
    tempsmoyen=(end-begin)/N
    return(tempsmoyen)



def graphe(nopmoy, nopnai):
    x=[]
    ymoy=[]
    ynai=[]

    for i in range(nopmoy):
        ymoy+=[moyenne(10**i)]

    for i in range(nopnai):
        ynai+=[moyennenaive(10**i)]

    for i in range(max(nopnai, nopmoy)):
        x+=[10**i]

    plot(x, ymoy, 'bo', label='methode DaC')
    plot(x, ynai, 'ro', label='methode naive')

    axis([10**-1, x[-1]*10, ynai[0]/10, ymoy[-1]*100 ])
    xlabel("nombre d'éléments")
    ylabel("temps [s]")
    
    xscale('log')
    yscale('log')

    legend()
    show()

    
graphe(6,6)



















    
