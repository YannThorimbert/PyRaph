import re
import os
 
files = None
s = GetActiveSource()
try:
    files = s.FileName
except AttributeError:
    print "Not a file source"
 
if files and len(files) == 1:
    # one time step
    s.FileName = [files[0]]
elif files:
    # multiple time steps
    reg = re.compile(r"(.+[^0-9])([0-9]+)\.(.+)")
    roots = set()
    exts = set()
    for f in files:
       m = reg.match(f)
       if m:
           roots.add(m.group(1))
           exts.add(m.group(3))
       else:
           roots = None
           break
    if roots and len(roots) == 1 and len(exts) == 1:
        root = roots.pop()
        ext = exts.pop()
        basedir = os.path.dirname(root)
        basename = os.path.basename(root)
        unsorted_l = []
        for p in os.listdir(basedir):
            m = reg.match(p)
            if m and basename == m.group(1):
                unsorted_l.append((int(m.group(2)), os.path.join(basedir, p)))
        s.FileName = [p for i, p in sorted(unsorted_l)]