from __future__ import division, print_function

#def get_children(state): #version plus performante
#    path = state[1] #state is on the form (cost, numbers)
#    h = len(path) + 1
#    children = []
#    for number in T:
#        newpath = path + [number]
#        newsum = sum(newpath)
#        if newsum == Q:
#            return [(0, newpath)]
#        elif newsum < Q:
#            cost = float(Q - newsum)/B + h
#            children.append((cost, newpath))
#    return children

def get_children(state):
    path = state[1] #state is on the form (cost, numbers)
    h = len(path) + 1
    children = []
    for number in T:
        newpath = path + [number]
        newsum = sum(newpath)
        if newsum <= Q:
            cost = float(Q - newsum)/B + h
            children.append((cost, newpath))
    return children

#def get_children(state): #version des etudiants
#    path = state[1] #state is on the form (cost, numbers)
#    h = len(path) + 1
#    children = []
#    for number in T:
#        newpath = path + [number]
#        newsum = sum(newpath)
#        if newsum <= Q:
#            cost = float(Q - newsum) + h
#            children.append((cost, newpath))
#    return children


def solve(lnl):
    enode = lnl.pop()
    while True:
        if sum(enode[1]) == Q: #if solution, returns
            return enode
        else:
            lnl += get_children(enode)
            if not lnl:
                print("No solution found.")
                return
            else:
                lnl.sort(reverse=True)
                enode = lnl.pop()
                print(enode)
                

def solve_greedy(): #pour verification que le probleme n'est pas trivial
    reste = Q
    sol = []
    while reste > 0:
        for n in T[::-1]:
            if n <= reste:
                reste -= n
                sol += [n]
    print("Greedy solution = ", sol)

T = [1,7,9]
Q = 14
B = max(T)
solution = solve([(float("inf"), [])]) #solve from initial state
solve_greedy()
