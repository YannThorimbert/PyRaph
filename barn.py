# -*- coding: utf-8 -*-
"""
Created on Wed Feb 25 10:15:46 2015

@author: yannthor
"""

import random
import numpy as np
from scipy.spatial import Delaunay

X = 100
Y = 100
N = 500
points = []
for i in range(N):
    x = random.randint(0, X)
    y = random.randint(0, Y)
    points.append((x, y))
points = np.array(points)
tri = Delaunay(points)
print tri.simplices.copy()
import matplotlib.pyplot as plt
plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
plt.plot(points[:,0], points[:,1], 'o')
plt.xlim([-1, X+1])
plt.ylim([-1, Y+1])
plt.show()

#filename_input = "./Downloads/bathymetrie/apres_amont.txt"
#filename_output = "./Downloads/bathymetrie/aa.txt"
#fi = open(filename_input, "r")
#fo = open(filename_output, "w")
#
#points = {}
##z = []
#
#for line in fi.readlines():
#    data = line[:-2]
#    point = data.split(" ")
#    x = float(point[0])
#    y = float(point[1])
#    z = float(point[2])
#    point2d = (x, y)
#    if point2d in points:
#        raise Exception("deja la...")
#    points[point2d] = z
#
#print "points ok"
#triangles_delaunay = Delaunay(points.keys())
#triangles = triangles_delaunay.simplices.copy()
#
#print "triangles ok"
#
#for triangle in triangles:
#    print triangle
#    for point in triangle:
#        print point
#        x = str(point[0])
#        y = str(point[1])
#        z = str(points[point])
#        fo.write(" ".join([x,y,z]) + "\n")
#
#fi.close()
#fo.close()




#filename_input = "./Downloads/bathymetrie/apres_amont.txt"
#filename_output = "./Downloads/bathymetrie/aa.txt"
#fi = open(filename_input, "r")
#fo = open(filename_output, "w")
#
#points = []
#
#for line in fi.readlines():
#    data = line[:-2]
#    point = data.split(" ")
#    x = float(point[0])
#    y = float(point[1])
#    z = float(point[2])
#    points.append([x, y, z])
#
#points = np.array(points)
#
#print "points ok"
#triangles_delaunay = Delaunay(points)
#triangles = points[triangles_delaunay.simplices.copy()]
#
#print "triangles ok"
#
#print "Min", np.min(triangles)
#print "Max", np.max(triangles)
#
#for triangle in triangles:
#    for point in triangle:
##        print point
#        x = str(point[0])
#        y = str(point[1])
#        z = str(point[2])
#        fo.write(" ".join([x,y,z]))
#    fo.write("\n")
#
#fi.close()
#fo.close()