# coding: utf8
from __future__ import print_function
"""Dans ce probleme on veut determiner toutes les combinaisons de nombres dont
la somme vaut 12. Les nombres sont pris parmi un ensemble donné (3,4 et 5).

On définit un état comme étant une liste de nombres."""

target_number = 12 #nombre a atteindre
numbers = [3, 4, 5] #liste de nombres que l'on peut utiliser

def get_children(state): #retourne les enfants d'un etat
    children = []
    for n in numbers:
        child = list(state) + [n] #l'enfant est l'etat auquel on ajoute n
        children.append(child)
    return children

def backtracking(state):
    if sum(state) == target_number:
        print(state)
    elif sum(state) < target_number:
        for child in get_children(state):
            backtracking(child)

initial_state = []
backtracking(initial_state)
