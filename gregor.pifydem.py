from __future__ import print_function, division
import math, random
import pygame

def get_lines(fn):
    f = open(fn, "r")
    t = None
    i = 0
    pts = {}
    for line in f.readlines():
        if i == 0:
            t = int(line)
        else:
            id_, x, y = line.split(",")
            pts[id_] = (int(x),int(y))
        i += 1
    return t, pts
    
def write_solution(fn, sol):
    f = open(fn, "w")
    for value in sol:
        f.write(value+"\n")
    f.close()


#greedy:

def get_dist(q, pos):
    return math.hypot(q[0]-pos[0], q[1]-pos[1])
    
def sort_pts(q, pts):
    dist = [(id_, get_dist(q, pos)) for id_, pos in pts.items()]
    dist = sorted(dist, key=lambda x : x[1])
    return dist
    
def solve_greedy(t, q, pts):
    to_eat = []
    time_elapsed = 0.
    while True:
        dist_sorted = sort_pts(q, pts)
        id_, distance = dist_sorted[0]
        time_elapsed += distance
        if time_elapsed > t:
            return to_eat
        else:
            q = pts[id_]
            to_eat.append(id_)
            pts.pop(id_)
            print(time_elapsed, len(to_eat))

def solve_greedy_clusters(t, q, pts, cscores):
    to_eat = []
    time_elapsed = 0.
    while True:
        s_sorted = sort_pts(q, pts)
        id_, distance = dist_sorted[0]
        time_elapsed += distance
        if time_elapsed > t:
            return to_eat
        else:
            q = pts[id_]
            to_eat.append(id_)
            pts.pop(id_)
            print(time_elapsed, len(to_eat))
    

def save_map(pts, k=0.1, eaten=[]):
    lpts = [(x,y) for x,y in pts.values()]
    xpts = [x for x,y in lpts]
    ypts = [y for x,y in lpts]
    xmin, xmax = min(xpts), max(xpts)
    ymin, ymax = min(ypts), max(ypts)
    print(xmin, xmax, ymin, ymax)
    #shift
    lpts = [(x-xmin, y-ymin) for x,y in lpts]
    w,h = xmax - xmin, ymax - ymin
    w,h = w*k, h*k
    surf = pygame.Surface((w,h))
    surf.fill((255,255,255))
    rect = pygame.Rect(0,0,3,3)
    for x,y in lpts:
        x *= k
        y *= k
        rect.center = (x,y)
        pygame.draw.rect(surf, (0,0,0), rect)
    linepts = []
    for key in eaten:
        x,y = pts[key]
        x,y = x - xmin, y - ymin
        x *= k
        y *= k
        linepts.append((x,y))
        rect.center = (x,y)
        pygame.draw.rect(surf, (0,0,255), rect)
    pygame.draw.lines(surf, (0,0,255), False, linepts)
    pygame.image.save(surf, "./mapagar.png")

def get_cluster_score(pts, id_, k):
    nearest = sort_pts(pts[id_], pts)
    assert nearest[0][0] == id_
    return sum([v[1] for v in nearest[1:k+1]])

def get_clustering_scores(pts, k):
##    return {id_:get_cluster_score(pts, id_, k) for id_ in pts}
    scores = {}
    i = 0
    for id_ in pts:
        if i%100 == 0:
            print(i)
        scores[id_] = get_cluster_score(pts, id_, k)
        i += 1


    
t, original_pts = get_lines("./input_2.txt")
q = (0, 0)
print("N:", len(original_pts))
pts = dict(original_pts)
##to_eat = solve_greedy(t, q, pts) #score : 991
##while True:
##    to_eat = []
##    pts = dict(original_pts)
##    to_eat = solve_greedy_random3(t, q, pts)
##    L = len(to_eat)
##    print(L)
##    if L > 991:
##        print("YEEEES")
##        write_solution("eat"+str(L)+".txt", to_eat)$

##save_map(original_pts, 0.1, to_eat)
print("Finding clusters ...")
cscores = get_clustering_scores(pts, 3)
