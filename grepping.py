import os, sys
from termcolor import colored
os.system("clear")

# pattern, root = sys.argv[1], sys.argv[2]
#
#
# command = "source kk '" + pattern + "' " + root + " >> result_rofl.dat"
# print(command)
# os.system(command)

filter2d = True
sort_by = "n"

results = {}

f = open("result_rofl.dat")

for line in f.readlines():
    r = line.split(":")
    fn = r[0]
    lineno = r[1]
    if filter2d and "2D" in line:
        continue
    if fn in results:
        results[fn].append(lineno)
    else:
        results[fn] = [lineno]

f.close()
paths = list(results.keys())
if sort_by == "path":
    paths.sort()
elif sort_by == "n":
    paths.sort(key = lambda x:len(results[x]), reverse=True)


for fn in paths:
    basename = os.path.basename(fn)
    root = os.path.dirname(fn)
    n = len(results[fn])
    print(colored(basename.ljust(40), "yellow"), colored(str("("+str(n)+")     ").ljust(10),"red"),  fn)
    # print("     ", " ".join(results[fn]))
