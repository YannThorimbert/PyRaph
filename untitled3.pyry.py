from __future__ import print_function, division
import pygame
import os


#path, fb, fe = "/home/yannthor/Downloads/c3/k2/", "h", ".png"
#path, fb, fe = "/home/yannthor/Downloads/c4/", "h", ".png"
path, fb, fe = "/home/yannthor/Downloads/canalysis/", "c", ".png"
fps = 24


f_imgs = [f for f in os.listdir(path) if f.startswith(fb) and f.endswith(fe)]
f_imgs.sort()
imgs = [pygame.image.load(path+f) for f in f_imgs]
n = len(imgs)
print(n, "images loaded.")
pygame.init()
size = imgs[0].get_size()
screen = pygame.display.set_mode(size)
imgs = [img.convert() for img in imgs]

#XLINE = 342
#for i in imgs:
#    pygame.draw.line(i, (0,255,0), (XLINE, 0), (XLINE, i.get_height()), 3)

i = 0
loop = True
clock = pygame.time.Clock()
pause = False
while loop:
    clock.tick(fps)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            loop = False
            break
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                pause = not pause
            if event.key == pygame.K_d:
                i = 0
    if not pause:
        screen.blit(imgs[i], (0,0))
        pygame.display.flip()
        i = (i+1)%n
        print(i*200*5e-5, i)

pygame.quit()

#for i,img in enumerate(imgs):
#    pygame.image.save(img, path+"/forgif/"+str(i).zfill(6)+".png")