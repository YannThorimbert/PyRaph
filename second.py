import time, os, random, datetime

min_wait = 1
max_wait = 17

while True:
    time2sleep = random.randint(min_wait*3600, max_wait*3600)
    print("Sleeping",time2sleep/3600., "at", datetime.datetime.now())
    time.sleep(time2sleep)
    ok = True
    if 0 <= datetime.datetime.now().hour <= 8:
        ok = random.random() < 0.2
    if ok:
        os.system("python3 first.py")

