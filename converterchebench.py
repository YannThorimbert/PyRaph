"""
Coding of a state : x = [x1, x2, x3]
means that there are queens on coordinates (0,x1), (1,x2), (2,x3).
Note that no line conflict is possible with this implicit line coding.
"""

def isThereConflict(k):
    #column conflict
    for col in x[:k]: #for each line..
        if col == x[k]: #..if a queen is on the same column..
            return True #..we must stop the exploration
    #diagonal conflicts
    for line, col in enumerate(x[:k]): #for any other queen..
        delta_x = abs(col-x[k])
        delta_y = abs(line-k)
        if delta_x == delta_y: #..if the queen is on the same diag..
            return True
    return False

def arret(k): #return True if no children should be generated
    if k==kmax-1:
        return True
    else:
        return isThereConflict(k)
    return False

def isResponse(k):
    if x.count(None) == 0:
        if not isThereConflict(k):
            return True
    return False

def isThereMoreToTry(k): #moreBrothers?
    if x[k]==xmax: return False
    return True

def next(k):
    if x[k]==None: return xmin
    return x[k]+1

# k max is the number of variables in x (here, kmax = N)
kmax=8
x=[None for i in range(kmax)]

# specify the range of value for each x[k]
xmax=kmax-1
xmin=0

# this is just to count the number of response
nbResponse=0

k=0
while(k>=0):  # program stops when it backtracked to k=-1
    if isThereMoreToTry(k):
        x[k]=next(k)
        if isResponse(k):
            nbResponse+=1
            print "response:", nbResponse, "val=", x[0:k+1]
    else:
        x[k]=None
        k=k-1
        continue

    if not arret(k):
        k=k+1
    else:
        continue

